pipeline {
    agent any
    parameters {
        gitParameter branchFilter: 'origin/(.*)', defaultValue: 'main', name: 'BRANCH', type: 'PT_BRANCH'
    }
        environment {
            DB_NAME     = "${DB_NAME}"
            DB_USERNAME = "${DB_USERNAME}"
            DB_URL      = "${DB_URL}"
            DB_PASSWORD = "${DB_PASSWORD}"
            DB_PORT     = "${DB_PORT}"
            NEXUS_VERSION       = "nexus3"
            NEXUS_PROTOCOL      = "http"
            NEXUS_URL           = "172.16.2.98:8081"
            NEXUS_GROUP_ID      = "gradle.project"
            NEXUS_CREDENTIAL_ID = "84c17074-3f60-4800-8f84-6ca9200ce1e2"
            NEXUS_REPO_BUILD    = "maven-nexus-build"
            NEXUS_REPO_RELEASE  = "maven-nexus-release"
            BUILD_VERSION       = "0.0.1-SNAPSHOT"
            RELEASE_VERSION     = "0.0.1-RELEASE"
            FILE_LOCATION       = "build/libs/demo-backend.jar"
            ARTIFACT_ID         = "backend"
        }
        stages {
            stage('Source') {
                steps {
                    git branch: "${params.BRANCH}", 
                    credentialsId: 'bd47a5c9-4d85-467a-abb0-1e44dd41c671', 
                    url: 'git@bitbucket.org:zuwarskej/devops-training-backend.git'
                }
            }
            stage('Test') {
                steps {
                    sh 'chmod +x gradlew'
                    sh './gradlew clean test --no-daemon'
                }
            }
            stage('Sonarqube') {
                environment {
                    scannerHome = tool 'SonarQubeScanner'
                }
                steps {
                    withSonarQubeEnv('sonarqube') {
                        sh "${scannerHome}/bin/sonar-scanner"
                    }
                    
                    timeout(time: 10, unit: 'MINUTES') {
                        waitForQualityGate abortPipeline: true
                    }
                }
            }
            stage('Build') {
                steps {
                    sh './gradlew clean build -x test --no-daemon'
                }
            }
            stage('Nexus: Publish Artifact') {
                steps {
                    script {
                        if ("${params.BRANCH}" == 'main') {
                            nexusArtifactUploader(
                                nexusVersion: NEXUS_VERSION,
                                protocol: NEXUS_PROTOCOL,
                                nexusUrl: NEXUS_URL,
                                version: RELEASE_VERSION,
                                groupId: NEXUS_GROUP_ID,
                                repository: NEXUS_REPO_RELEASE,
                                credentialsId: NEXUS_CREDENTIAL_ID,
                                artifacts: [
                                    [   artifactId: ARTIFACT_ID,
                                        classifier: '',
                                        file: FILE_LOCATION, 
                                        type: 'jar'
                                    ]
                                ]
                            )
                        } 
                        if ("${params.BRANCH}" == 'dev') {
                            nexusArtifactUploader(
                                nexusVersion: NEXUS_VERSION,
                                protocol: NEXUS_PROTOCOL,
                                nexusUrl: NEXUS_URL,
                                version: BUILD_VERSION,
                                groupId: NEXUS_GROUP_ID,
                                repository: NEXUS_REPO_BUILD,
                                credentialsId: NEXUS_CREDENTIAL_ID,
                                artifacts: [
                                    [   artifactId: ARTIFACT_ID,
                                        classifier: '',
                                        file: FILE_LOCATION, 
                                        type: 'jar'
                                    ]
                                ]
                            )
                        }
                    }
                }
            }
        }
        post {
            always {
                echo "Clean Complete"
                cleanWs()
            }
            failure {
                echo "Clean Failed"
            }
            success {
                echo "Clean Succeeded"
            }
        }
    }
