### STAGE 1: Build ###
FROM gradle:4.7.0-jdk8-alpine AS build
WORKDIR /backend
ADD . .
USER root
RUN apk update && apk upgrade && \
    apk add --no-cache bash nss openssl > /dev/null 2>&1 && \
    rm -rf /var/cache/apk/* /tmp/* /var/tmp/* /etc/apk/cache

RUN if [ ! -f ./gradlew ]; then \
        gradle --quiet --no-daemon --no-build-cache wrapper; \
    fi 
RUN ./gradlew --quiet --no-daemon --no-build-cache build -x test && ls

### STAGE 2: Run ###
FROM openjdk:8-jre-slim AS run
WORKDIR /opt/backend
RUN useradd -ms /bin/bash backend && chown -R backend .
USER backend

ENV BUILD_DIR="/backend/build" \
    APP_DIR="/opt/backend" \
    APP_NAME="backend.jar"

COPY --from=build --chown=backend:backend ${BUILD_DIR}/libs/* ${APP_DIR}/${APP_NAME}
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "backend.jar" ]